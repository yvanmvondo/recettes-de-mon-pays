import 'package:flutter/material.dart';
import 'package:appetit/pages/bienvenue/bienvenue.dart';
import 'constantes.dart';

void main() =>
  runApp(MyApp());


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Authentification',
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: kPrimaryColor,
        scaffoldBackgroundColor: kBackgroundColor,
        textTheme: TextTheme(
          headline3: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          button: TextStyle(color: kPrimaryColor),
          subtitle1:
          TextStyle(color: Colors.white, fontWeight: FontWeight.normal),
        )
        ),
      home: Bienvenue(),
    );
  }
}